import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MenubarModule} from "primeng/menubar";
import {AvatarModule} from 'primeng/avatar';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {DividerModule} from "primeng/divider";
import {HeaderMenuComponent} from './shared/header-menu/header-menu.component';
import {SharedModule} from "primeng/api";
import {CardModule} from 'primeng/card';
import {SelectButtonModule} from 'primeng/selectbutton';
import {CalendarModule} from 'primeng/calendar';
import {CheckboxModule} from 'primeng/checkbox';
import {DialogModule} from 'primeng/dialog';
import {TabViewModule} from 'primeng/tabview';
import {NgxsModule} from '@ngxs/store';
import {RippleModule} from 'primeng/ripple';
import {StudentState} from './actions/student.action';
import {ModuleState} from './actions/module.action';
import {IntervenantState} from './actions/intervenant.action';
import {CommonModule, registerLocaleData} from '@angular/common';

import {FullCalendarModule} from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import {StatusComponent} from "./shared/status/status.component";
import {CardComponent} from "./shared/card/card.component";
import {SelectableCardComponent} from "./shared/selectable-card/selectable-card.component";
import {InputTextModule} from "primeng/inputtext";
import {ButtonModule} from "primeng/button";
import {ListboxModule} from "primeng/listbox";
import {FormsModule} from "@angular/forms";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {LoginComponent} from "./login/login.component";
import {DetailComponent} from "./module/detail/detail.component";
import {ListComponent} from "./module/list/list.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SignatureModule} from "@syncfusion/ej2-angular-inputs";
import localeFr from '@angular/common/locales/fr';
import {NgxsStoragePluginModule} from "@ngxs/storage-plugin";
import {SignatureState} from "./actions/signature.action";
import {ProgressSpinnerModule} from "primeng/progressspinner";
import { TimeCountDownComponent } from './shared/time-count-down/time-count-down.component';

registerLocaleData(localeFr);


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin
]);


@NgModule({
  declarations:[
    AppComponent,
    HeaderMenuComponent,
    StatusComponent,
    CardComponent,
    SelectableCardComponent,
    DashboardComponent,
    LoginComponent,
    DetailComponent,
    ListComponent,
    TimeCountDownComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MenubarModule,
    AvatarModule,
    DividerModule,
    ButtonModule,
    SharedModule,
    ToggleButtonModule,
    FormsModule,
    CardModule,
    SelectButtonModule,
    FullCalendarModule,
    CalendarModule,
    CheckboxModule,
    DialogModule,
    CommonModule,
    TabViewModule,
    NgxsModule.forRoot([StudentState, ModuleState, IntervenantState, SignatureState]),
    NgxsStoragePluginModule.forRoot(),
    InputTextModule,
    RippleModule,
    ListboxModule,
    SignatureModule,
    ProgressSpinnerModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: "fr-FR"}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
