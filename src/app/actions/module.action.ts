import {Module} from "../structure/module";
import {Action, Select, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';

export namespace module {
  export class Add {
    static readonly type = '[module] Add';

    constructor(public module: Module) {
    }
  }
  export class AddMultiple{
    static readonly type = '[module] AddMultiple';

    constructor(public modules: Module[]) {
    }
  }
  export class Selected {
    static readonly type = '[module] Selected'

    constructor(public moduleId: number) {
    }
  }
}

export interface Modules {
  modules: Module[];
  selected: number;
}

/* The above class is a state class that has a method that adds a module to the state. */
@State<Modules>({
  name: 'modules',
  defaults: {
    modules: [],
    selected: -1
  }
})
@Injectable()
export class ModuleState {
  @Action(module.Add)
  addModule(ctx: StateContext<Modules>, action: module.Add) {
    const oldModuleState = ctx.getState();
    const oldModuleArray = [...oldModuleState.modules];
    oldModuleArray.push(action.module);
    ctx.setState({
      ...oldModuleState,
      modules: [...oldModuleArray]
    })
  }

  @Action(module.AddMultiple)
  addMultipleModule(ctx: StateContext<Modules>, action: module.AddMultiple) {
    const oldModuleState = ctx.getState();
    const oldModuleArray = [...oldModuleState.modules];
    oldModuleArray.push(...action.modules);
    ctx.setState({
      ...oldModuleState,
      modules: [...oldModuleArray]
    })
  }

  @Action(module.Selected)
  changeSelectedModule({patchState}: StateContext<Modules>, action: module.Selected) {
    patchState({selected: action.moduleId});
  }

  @Selector()
  static getModules(state: Modules): Module[]{
    return state.modules;
  }

  @Selector()
  static selectedModule(state: Modules): Module{
    return state.modules.find((el) => el.id === state.selected);
  }

  @Selector()
  static endDate(state: Modules): Date{
    return state.modules.find((el) => el.id === state.selected).lessons[0].endDate
  }
}
