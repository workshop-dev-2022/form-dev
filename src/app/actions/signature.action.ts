import {Action, State, StateContext} from "@ngxs/store";
import {Injectable} from "@angular/core";
import {Student} from "../structure/student";

export namespace Signature {
  export class SaveSignature {
    static readonly type = '[Signature] save signatures'
    constructor(public signatureUrlData: string,public moduleId: number,public lessonId: number, public student: Student) {}
  }
}

export interface Signatures{
  lessonsToBeSigned: LessonRelatedSignature[]
}

export type LessonRelatedSignature = {
  data: string,
  moduleId: number,
  lessonId: number
  student: Student
}

@State<Signatures>({
  name: 'signature',
  defaults: {
    lessonsToBeSigned : []
  }
})
@Injectable()
export class SignatureState{
  @Action(Signature.SaveSignature)
  saveSignature(ctx: StateContext<Signatures>, action: Signature.SaveSignature){
    const oldState = ctx.getState();
    const oldStateCopy = [...oldState.lessonsToBeSigned];

    oldStateCopy.push({data: action.signatureUrlData, moduleId: action.moduleId, lessonId: action.lessonId, student: action.student})
    ctx.setState({lessonsToBeSigned : [...oldStateCopy]})
  }
}
