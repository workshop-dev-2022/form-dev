import {Intervenant} from "../structure/intervenant";
import {State, Action, StateContext, Select, Selector, createSelector} from '@ngxs/store';
import {Injectable} from '@angular/core';

export namespace intervenant {
  export class Add {
    static readonly type = '[intervenant] Add';

    constructor(public intervenant: Intervenant) {
    }
  }

  export class AddMultiple{
    static readonly type = '[intervenant] add multiple';
    constructor(public intervenants: Intervenant[]) {}
  }
}

export interface Intervenants {
  intervenants: Intervenant[];
}

/* The class is a state class that is used to store the data of the intervenants. */
@State<Intervenants>({
  name: 'intervenant',
  defaults: {
    intervenants: []
  }
})
@Injectable()
export class IntervenantState {
  @Action(intervenant.Add)
  addIntervenant(ctx: StateContext<Intervenants>, action: intervenant.Add) {
    const oldIntervenantState = ctx.getState();
    /*console.log(oldModuleState)*/
    const oldIntervenantArray = [...oldIntervenantState.intervenants];
    oldIntervenantArray.push(action.intervenant);
    ctx.setState({
      ...oldIntervenantState,
      intervenants: [...oldIntervenantArray]
    })
  };

  @Action(intervenant.AddMultiple)
  addIntervenants(ctx: StateContext<Intervenants>, action: intervenant.AddMultiple){
    const oldIntervenantState = ctx.getState();
    /*console.log(oldModuleState)*/
    const oldIntervenantArray = [...oldIntervenantState.intervenants];
    oldIntervenantArray.push(...action.intervenants);
    ctx.setState({
      ...oldIntervenantState,
      intervenants: [...oldIntervenantArray]
    })
  }

  @Selector()
  static intervenants(state: Intervenants) {
    console.log(state)
    return state.intervenants
  }
}
