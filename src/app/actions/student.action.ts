import {Student} from "../structure/student";
import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';

export namespace student {
    export class Add {
        static readonly type = '[student] Add';
        constructor(public student: Student) { }
    }
}

export interface Students {
    students: Student[];
}

/* The StudentState class is a state class that has a single action called Add. The Add action takes a
student object and adds it to the students array. */
@State<Students>({
    name: 'students',
    defaults: {
        students: []
    }
})
@Injectable()
export class StudentState {
    @Action(student.Add)
    addStudent(ctx: StateContext<Students>, action: student.Add) {
        const oldState = ctx.getState();
        /*console.log(oldState)*/
        const oldStudentArray = [...oldState.students];
        oldStudentArray.push(action.student);
        ctx.setState({
            ...oldState,
            students: [...oldStudentArray]
        })
    }
}
