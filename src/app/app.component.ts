import {Component} from '@angular/core';
import {PrimeNGConfig} from 'primeng/api';
import {fromEvent, iif, mergeMap, Observable, of, tap} from "rxjs";
import {Select} from "@ngxs/store";
import {Signatures, SignatureState} from "./actions/signature.action";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private primengConfig: PrimeNGConfig) {

  }
  hasToSaveOnConnectionUp: boolean = false
  @Select(SignatureState) signature: Observable<Signatures>;
  ngOnInit() {

    this.primengConfig.ripple = true;
    fromEvent(window, 'offline').pipe(tap(_ => this.hasToSaveOnConnectionUp = true)).subscribe()
    fromEvent(window, 'online').pipe(
      mergeMap(() => iif(() => this.hasToSaveOnConnectionUp, this.sendAllSignature(), of(void 0))),
    ).subscribe()
  }

  private sendAllSignature(): Observable<Signatures>{
    return this.signature.pipe(tap(console.log), tap(_ => this.hasToSaveOnConnectionUp = false));
  }
  title = 'form-dev';
}
