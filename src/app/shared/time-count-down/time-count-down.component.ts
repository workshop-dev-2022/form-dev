import { Component, Input, OnInit } from '@angular/core';
import {interval, map, mergeMap, Observable, take, timer} from "rxjs";
import * as moment from "moment";
import {Select} from "@ngxs/store";
import {ModuleState} from "../../actions/module.action";

@Component({
  selector: 'app-time-count-down',
  templateUrl: './time-count-down.component.html',
  styleUrls: ['./time-count-down.component.scss']
})
export class TimeCountDownComponent implements OnInit {

  @Select(ModuleState.endDate) endDate: Observable<Date>;
  countDown$ : Observable<string> = new Observable<string>()


  constructor() { }

  ngOnInit(): void {
    this.countDown$ = timer(0,1000)
      .pipe(
        mergeMap(_ => this.endDate),
        map((date) =>
          moment({...date}).subtract(Date.now())
            .format('HH:mm:ss'))
      )
  }

}
