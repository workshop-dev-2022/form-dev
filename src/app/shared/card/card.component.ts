import {Component, Input, OnInit} from '@angular/core';
import {Student} from "../../structure/student";
import {StudentWithStatus} from "../status/status.component";


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() studentInput : Student;
  student : StudentWithStatus;
  constructor() { }

  ngOnInit(): void {
    this.student = {...this.studentInput, status: 'EN_ATTENTE' }
  }

}
