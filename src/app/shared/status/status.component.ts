import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Module} from "../../structure/module";
import {Student} from "../../structure/student";
import {Select, Store} from "@ngxs/store";
import {Signature} from "../../actions/signature.action";
import {ModuleState} from "../../actions/module.action";

type Mode = 'Terminal' | 'Mail' | 'QR'

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {


  @Select(ModuleState.selectedModule) module$: Observable<Module>;
  @Input() mode: Mode = 'Terminal'

  constructor(private store: Store) { }

  ngOnInit(): void {

  }
  consollog = console.log

  log($event: MouseEvent) {
    console.log($event)
  }


  saveSignature($event: string, moduleId: number, eleve: Student) {
    // TODO changer le lessonId par le bon / inverser la condition du navigator / chercher le student qui a signer et le changer d'état
    if(navigator.onLine){
      this.store.dispatch(new Signature.SaveSignature($event, moduleId,0, eleve))

    }
  }
}

export type StudentModuleStatus = 'EN_ATTENTE' | 'PRESENT' | 'ABSENT'

export type StudentWithStatus = Student & {status :  StudentModuleStatus };
