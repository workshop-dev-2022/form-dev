import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {SignatureComponent} from "@syncfusion/ej2-angular-inputs";
import {Student} from "../../structure/student";
import {map, tap, timer} from "rxjs";

@Component({
  selector: 'app-selectable-card',
  templateUrl: './selectable-card.component.html',
  styleUrls: ['./selectable-card.component.scss']
})
export class SelectableCardComponent implements OnInit {

  @Input() student: Student = {} as Student
  isDialogVisible: boolean = false;

  @ViewChild('signature')
  private signature: SignatureComponent | undefined;

  @Output('onSignFinished') onSignFinished : EventEmitter<string> = new EventEmitter<string>();
  showSpinner: boolean = false;

  isButtonDisplayed : boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

  openDialog(){
    this.isDialogVisible = true;
  }

  closeDialog(){
    this.isDialogVisible = false;
  }

  save(){
    this.showSpinner = true
    this.onSignFinished.emit(this.signature?.getSignature());
    timer(1000)
      .pipe(
        map(this.closeDialog.bind(this)),
        tap(_ => this.isButtonDisplayed = false)
        )
      .subscribe()
  }

  clear() {
    this.signature.clear();
  }
}
