import { Module } from './../../structure/module';
import { Router, ActivatedRoute } from '@angular/router';
import { IntervenantService } from 'src/app/services/intervenant.service';
import { Component, Input, OnInit } from '@angular/core';
import { ModuleService } from 'src/app/services/module.service';
import { CalendarOptions } from '@fullcalendar/core';
import {Select, Store} from "@ngxs/store";
import {module, ModuleState} from "../../actions/module.action";
import {Observable, take, tap} from "rxjs";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {

  stateOptions: any[];

  typeList: string = 'Card';

  events: any[] = [];

  calendarOptions: CalendarOptions = {};

  @Select(ModuleState.getModules) modules: Observable<Module[]>;
  /**
   * The constructor is a function that is called when a new instance of the class is created. It is
   * used to initialize the instance members of the class.
   * @param {IntervenantService} intervenantService - IntervenantService, moduleService: ModuleService,
   * private router: Router, private activatedRoute: ActivatedRoute
   * @param store
   * @param {Router} router - Router, private activatedRoute: ActivatedRoute
   * @param {ActivatedRoute} activatedRoute - ActivatedRoute
   */
  constructor(public intervenantService: IntervenantService, private store: Store,private router: Router, private activatedRoute: ActivatedRoute) {

    this.stateOptions = [
      { icon: 'pi pi-list', value: 'Card' },
      { icon: 'pi pi-calendar', value: 'Calendar' },
    ];


  }

  /* Setting the calendar options. */
  ngOnInit() {
    this.setEvents(this.modules);
    let self = this;
    this.calendarOptions = {
      initialView: 'timeGridWeek',
      weekends: true,
      locale: 'frLocale',
      titleFormat:
        { year: 'numeric', month: 'long', day: 'numeric' },
      headerToolbar: {
        start: "title",
      },
      buttonText: {
        today: "Aujourd'hui"
      },
      height: 800,
      expandRows: true,
      slotMinTime: "08:00:00",
      slotMaxTime: "18:00:00",
      weekNumbers: true,
      weekText: "S",
      nowIndicator: true,
      selectable: true,
      eventClick: function ({event}) {
        self.store.dispatch(new module.Selected(Number(event.id)))
        self.router.navigate(['details'], { relativeTo: self.activatedRoute })
      }


    };

    this.calendarOptions.events = [{
      title: '3 jours',
      start: '2022-06-13 09:00',
      end: '2022-06-15 09:00',
    },
    { title: 'Dentiste', date: '2022-06-13' },
    { title: 'event 2', date: '2022-06-16' }];

    this.calendarOptions.events = this.events;
  }


  /**
   * This function is used to set the events for the calendar.
   */
  private setEvents(modules: Observable<Module[]>) {
    modules
      .pipe(tap(console.log))
      .pipe(take(1))
      .subscribe(modules => {
        this.events = modules.map(module => module.lessons.map(lesson => ({
          id: module.id,
          title: module.title,
          start: lesson.startDate,
          end: lesson.endDate
        }))).flat()
      })
  }

  /**
   * This function navigates to the details page, and passes the module object as a state parameter.
   * @param {Module} module - Module - this is the object that I want to pass to the details page
   */
  public goToDetails(module: Module) {
    this.router.navigate(['details'], { relativeTo: this.activatedRoute, state: { module: module } })
  }
}
