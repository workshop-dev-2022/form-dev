import {ActivatedRoute, Router} from '@angular/router';
import {Module} from './../../structure/module';
import {Component, OnInit} from '@angular/core';
import {Student} from 'src/app/structure/student';
import {Select, Store} from "@ngxs/store";
import {module, ModuleState} from "../../actions/module.action"
import {Observable} from "rxjs";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Select(ModuleState.selectedModule) module$: Observable<Module>;

  selectedStudents!: Student[];

  state: any;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private store: Store
  ) {
    this.state = this.router.getCurrentNavigation()?.extras.state;
    console.log("detail : ", this.module$);
  }

  ngOnInit(): void {
  }

  goToSignaturePage(id: number) {
    this.store.dispatch(new module.Selected(id))
    this.router.navigate(['/dashboard'])
  }
}
