import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {combineLatest, defer, forkJoin, iif, map, mergeMap, Observable, of, pluck, tap} from 'rxjs';
import {ModuleService} from "../services/module.service";
import {IntervenantService} from "../services/intervenant.service";
import {Select} from "@ngxs/store";
import {Intervenants, IntervenantState} from "../actions/intervenant.action";
import {Modules, ModuleState} from "../actions/module.action";

@Injectable({
  providedIn: 'root'
})
export class InitGuard implements CanActivate {
  isGuardPassed: boolean = false;
  @Select(IntervenantState) intervenants: Observable<Intervenants>;
  @Select(ModuleState) modules : Observable<Modules>

  constructor(private moduleService: ModuleService,
              private intervenantService: IntervenantService
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.isGuardPassed ? of(true) :
      this.init()
        .pipe(
          tap(val => (this.isGuardPassed = val)),
          map(() => true)
        )
  }

  private init(): Observable<boolean> {
    return of(void 0)
      .pipe(
        mergeMap(() => this.intervenants),
        mergeMap((intervenant) => iif(() => !!intervenant.intervenants.length, this.intervenants, defer(() => {
          this.intervenantService.generateIntervenants();
          return this.intervenants
        }))),
        mergeMap((_) => combineLatest([this.intervenants, this.modules.pipe(pluck('modules'))])),
        tap(console.log),
        mergeMap(([inter, modu]) => iif(() => !!modu.length, this.modules, defer(() => {
            this.moduleService.generateModules(inter.intervenants)
            return this.modules
          })
        )),
        map(() => true)
      );
  }
}
