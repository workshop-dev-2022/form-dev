import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { IntervenantState } from '../actions/intervenant.action';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  logins: any;

  // @Select(IntervenantState.selectIntervenant('Manon'))
  // intervenant: Observable<string[]> = new Observable();

  constructor(private router: Router, private store: Store) {

    fetch("./assets/test.json").then(res => res.json()).then(data => {
      this.logins = data;
    })


    // console.log(this.intervenant$);

  }

  ngOnInit(): void {
  }

  /**
   * It checks if the email and password entered by the user matches the email and password in the
   * logins array. If it does, it navigates to the modules page
   * @param {string} password - string, email: string
   * @param {string} email - string, password: string
   */
  signIn(password: string, email: string) {
    const successful = this.logins.find(((element: { email: string; password: string; }) => element.email == email && element.password == password));
    console.log(successful);
    if (successful != undefined) {
      this.router.navigate(["modules"]);
    }
    else console.log("Failed");

  }

}