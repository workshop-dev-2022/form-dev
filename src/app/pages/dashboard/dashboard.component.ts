import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import { DashboardService } from "./dashboard.service";
import { Observable, of, Subject, switchMap, takeUntil } from "rxjs";
import { Module } from "../../structure/module";
import { Select } from "@ngxs/store";
import { Modules, ModuleState } from '../../actions/module.action'
import {DateTime}  from 'luxon';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  destroy$: Subject<void> = new Subject<void>();
  @Select(ModuleState) public moduleState$: Observable<Modules>;

  selectedModule: Observable<Module>;
  constructor() {
  }
  result (){

  }
  /**
   * When the moduleState changes, take the value of the modules array and find the module with the
   * same id as the selected module.
   */
  ngOnInit() {
    this.selectedModule = this.moduleState$.pipe(
      takeUntil(this.destroy$),
      switchMap(value => of(value.modules.find(el => el.id === value.selected))))
  }

  /**
   * When the component is destroyed, we want to complete the observable so that any subscriptions to
   * it will be unsubscribed.
   */
  ngOnDestroy() {
    this.destroy$.next()
    this.destroy$.complete()
  }

  /**
   * The back() function is used to go back to the previous page.
   */
  back() {
    window.history.back();
  }

  /**
   * Remove licence banner, don't forget to buy a licence...
   */
  ngAfterViewInit(): void {
    const el = document.querySelectorAll('#js-licensing')[0];
    const parent = el.parentElement;
    parent.removeChild(el);
  }

  getSelectedModule(module: Module[], selected: number ){
    return {...module.find((el) => el.id === selected)}
  }

}
