import {Student} from './student';

export interface Module {
  id: number;
  title: String;
  students: Student[];
  intervenantId: number;
  lessons: Lesson[]
}
export interface Lesson{
  id: number,
  moduleId: number,
  startDate: Date,
  endDate: Date
}
