import { DetailComponent } from './module/detail/detail.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ListComponent } from './module/list/list.component';
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {InitGuard} from "./guard/init.guard";

const routes: Routes = [
  {
    path: '',
    canActivate: [InitGuard],
    children: [
      { path: "", redirectTo: "/login", pathMatch: "full" },
      { path: "login", component: LoginComponent },
      {
        path: 'modules', component: ListComponent
      },
      { path: "modules/details", component: DetailComponent },
      {path: "dashboard", component: DashboardComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}

