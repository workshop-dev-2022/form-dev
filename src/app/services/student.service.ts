import {Injectable} from '@angular/core';
import {Student} from '../structure/student';
import {Store} from '@ngxs/store';
import {student} from '../actions/student.action';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private _firstNames: string[] = [
    "Romain",
    "Nicolas",
    "Eve",
    "Faissoil",
    "Pierre",
    "Manon",
    "Laetitia",
    "Julien",
    "Bertini",
    "Flavien",
    "Rémi",
    "Léa",
    "Jonathan",
    "Matéo",
    "Alan",
    "Merveille",
    "Lola",
    "Allison",
    "Sarah",
    "Margaux",
    "Ghislain",
    "David",
    "Lucas",
    "Enzo",
    "Tangui",
    "Vincent",
    "Alexandre",
    "Steffen",
    "Jayro",
    "Bastien",
    "Hugo"
  ];

  private _lastNames: string[] = [
    "Martin",
    "Bernard",
    "Thomas",
    "Petit",
    "Robert",
    "Richard",
    "Durand",
    "Dubois",
    "Moreau",
    "Laurent",
    "Simon",
    "Michel",
    "Lefebvre",
    "Leroy",
    "Roux",
    "David",
    "Bertrand",
    "Morel",
    "Fournier",
    "Girard",
    "Bonnet",
    "Dupont",
    "Lambert",
    "Fontaine",
    "Rousseau",
    "Vincent",
    "Muller",
    "Faure",
    "Andre",
    "Mercier",
    "Blanc",
    "Guerin",
    "Boyer",
    "Garnier",
    "Chevalier",
    "Francois",
    "Legrand",
    "Gauthier",
    "Garcia",
    "Perrin",
    "Robin"
  ];

  /**
   * A constructor function that takes in a store object and assigns it to the store variable.
   * @param {Store} store - Store - The store is the single source of truth for the application state.
   */
  constructor(private store: Store) {
  }

  /**
   * It generates a random number of students, with random names, emails, and grades, and then
   * dispatches an action to add each student to the store.
   * @param {number} maxStudents - number - the number of students to generate
   * @param {String} schoolClass - String
   * @returns An array of students.
   */
  public generateStudents(maxStudents: number, schoolClass: string) {
    let students: Student[] = [];
    for (let i = 0; i < maxStudents; i++) {
      let firstName: string = this._firstNames[Math.floor(Math.random() * this._firstNames.length)];
      let lastName: string = this._lastNames[Math.floor(Math.random() * this._lastNames.length)];
      let email: string = firstName + '.' + lastName + "@gmail.com";
      students[i] = {firstName, lastName, email, schoolClass, id: Math.floor(Math.random() * 100)}
      this.store.dispatch(new student.Add(students[i]))
    }

    return students;
  }
}
