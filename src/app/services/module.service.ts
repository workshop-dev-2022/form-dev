import { Intervenant } from './../structure/intervenant';
import { Injectable } from '@angular/core';
import { Module } from '../structure/module';
import { StudentService } from './student.service';
import { Store } from '@ngxs/store';
import { module } from '../actions/module.action';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  public modules: Module[] = [];
  private availableDays: { startDate: Date, endDate: Date }[] = [];

  /**
   * The constructor function is called when the class is instantiated
   */
  constructor(private studentService: StudentService, private store: Store) {
    const now = new Date();
    for (let i = 0; i < 20; i++) {
      const startDate = new Date()
      const endDate = new Date()

      startDate.setDate(now.getDate() + i)
      endDate.setDate(now.getDate() + i)

      startDate.setUTCHours(8, 0, 0, 0);
      endDate.setUTCHours(18, 0, 0, 0);
      this.availableDays.push({ startDate, endDate })
    }
  }

  /**
   * It returns a random date between two dates
   * @param start - The start date of the range.
   * @param end - { getTime: () => number; }
   * @returns A random date between the start and end dates.
   */
  private randomDate(start: { getTime: () => number; }, end: { getTime: () => number; }) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }

  /**
   * It generates 10 modules with random dates, random students and random teachers
   * @param {Intervenant[]} intervenants - Intervenant[]
   */
  public generateModules(intervenants: Intervenant[]) {
    for (let i = 0; i < 10; i++) {
      let title: string = "Module " + i;
      let students = this.studentService.generateStudents(20, "Classe 1");
      let intervenant: Intervenant = intervenants[Math.floor(Math.random() * intervenants.length)];
      let moduleId = Math.floor(Math.random() * 100)
      let lessonId = Math.floor(Math.random() * 100)
      this.modules[i] = {
        students,
        title,
        intervenantId: intervenant.id,
        id: moduleId,
        lessons: this.availableDays.splice(0, 2).map(el => ({ id: lessonId, moduleId, endDate: el.endDate, startDate: el.startDate })),
      }
    }
    this.store.dispatch(new module.AddMultiple(this.modules))
  }
}
