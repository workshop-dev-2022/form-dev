import { Injectable } from '@angular/core';
import { Intervenant } from '../structure/intervenant';
import { ModuleService } from './module.service';
import { Store } from '@ngxs/store';
import { intervenant } from '../actions/intervenant.action';

@Injectable({
  providedIn: 'root'
})
export class IntervenantService {

  public intervenants: Intervenant[] = [];
  private _firstNames: string[] = [
    "Romain",
    "Nicolas",
    "Eve",
    "Faissoil",
    "Pierre",
    "Manon",
    "Laetitia",
    "Julien",
    "Bertini",
    "Flavien",
    "Rémi",
    "Léa",
    "Jonathan",
    "Matéo",
    "Alan",
    "Merveille",
    "Lola",
    "Allison",
    "Sarah",
    "Margaux",
    "Ghislain",
    "David",
    "Lucas",
    "Enzo",
    "Tangui",
    "Vincent",
    "Alexandre",
    "Steffen",
    "Jayro",
    "Bastien",
    "Hugo"
  ];

  private _lastNames: string[] = [
    "Martin",
    "Bernard",
    "Thomas",
    "Petit",
    "Robert",
    "Richard",
    "Durand",
    "Dubois",
    "Moreau",
    "Laurent",
    "Simon",
    "Michel",
    "Lefebvre",
    "Leroy",
    "Roux",
    "David",
    "Bertrand",
    "Morel",
    "Fournier",
    "Girard",
    "Bonnet",
    "Dupont",
    "Lambert",
    "Fontaine",
    "Rousseau",
    "Vincent",
    "Muller",
    "Faure",
    "Andre",
    "Mercier",
    "Blanc",
    "Guerin",
    "Boyer",
    "Garnier",
    "Chevalier",
    "Francois",
    "Legrand",
    "Gauthier",
    "Garcia",
    "Perrin",
    "Robin"
  ];

  /**
   * The constructor function is called when the component is instantiated, and it's used to initialize
   * class members.
   * @param {ModuleService} moduleService - ModuleService
   * @param {Store} store - Store is an object that contains the data that I want to display in the
   * view.
   */
  constructor(moduleService: ModuleService, private store: Store) {
  }

  /**
   * It generates 5 random intervenants and adds them to the store
   */
  public generateIntervenants() {
    for (let i = 0; i < 5; i++) {
      let firstName: string = this._firstNames[Math.floor(Math.random() * this._firstNames.length)];
      let lastName: string = this._lastNames[Math.floor(Math.random() * this._lastNames.length)];
      this.intervenants[i] = {id: Math.floor(Math.random() * 100), firstName, lastName, password:"Admin"}
    }
    this.store.dispatch(new intervenant.AddMultiple(this.intervenants))
  }
}
